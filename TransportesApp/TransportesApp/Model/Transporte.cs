﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TransportesApp.Intefaces;

namespace TransportesApp.Model
{
    public class Transporte
    {
        private Engine Engine;
        private Driver Driver;

        public void setEngine(Engine engine)
        {
            this.Engine = engine;
        }

        public void setDriver(Driver driver)
        {
            this.Driver = driver;
        }

        public void move()
        {
            if (Engine == null)
            {
                MessageBox.Show("no se ha seleccionado motor");
                return;
            }

            Engine.move();


            if (Driver == null)
            {
                MessageBox.Show("no se ha seleccionado conductor");
                
            }
            else
            {
                Driver.navigate();
            }
        }
    }
}
