using TransportesApp.Intefaces;
using TransportesApp.Model;

namespace TransportesApp
{
    public partial class Form1 : Form
    {
        private Transporte transporte;
        private Engine selectedEngine;
        private Driver selectedDriver;
        public Form1()
        {
            InitializeComponent();
        }

        private void BtnAddDriver_Click(object sender, EventArgs e)
        {
            if (RbHuman.Checked)
            {
                selectedDriver = new Human();
                MessageBox.Show("conductor humano seleccionado");
            }
            else if (RbRobot.Checked)
            {
                selectedDriver = new Robot();
                MessageBox.Show("conductor robot seleccionado");
            }
            else
            {
                MessageBox.Show("Por favor seleccione conductor");
            }
            updateTranport();
        }
        private void BtnAddEngine_Click(object sender, EventArgs e)
        {
            if (RbCombustion.Checked) 
            {
                selectedEngine = new CombustionEngine();
                MessageBox.Show("Motor a combustion seleccionado");
            }
            else if (RbElectric.Checked)
            {
                selectedEngine = new ElectricEngine();
                MessageBox.Show("Motor electrico seleccionado");
            }
            else
            {
                MessageBox.Show("Por favor seleccione motor");
            }
            updateTranport();
        }
        private void updateTranport()
        {
            transporte = new Transporte();
            transporte.setEngine(selectedEngine);
            transporte.setDriver(selectedDriver);
        }
        private void BtnStartDrive_Click(object sender, EventArgs e)
        {
            if (selectedEngine == null)
            {
                MessageBox.Show("por favor seleccione motor");
                return;
            }
            if (selectedDriver == null)
            {
                MessageBox.Show("por favor seleccione conductor");
                return;
            }
            transporte.move();
        }

        
    }
}
