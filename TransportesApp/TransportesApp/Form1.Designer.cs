﻿namespace TransportesApp
{
    partial class Form1
    {
        /// <summary>
        ///  Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        ///  Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        ///  Required method for Designer support - do not modify
        ///  the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            BtnAddEngine = new Button();
            BtnAddDriver = new Button();
            BtnStartDrive = new Button();
            RbHuman = new RadioButton();
            RbRobot = new RadioButton();
            RbElectric = new RadioButton();
            RbCombustion = new RadioButton();
            GboxEngine = new GroupBox();
            GboxDriver = new GroupBox();
            GboxEngine.SuspendLayout();
            GboxDriver.SuspendLayout();
            SuspendLayout();
            // 
            // BtnAddEngine
            // 
            BtnAddEngine.Location = new Point(226, 132);
            BtnAddEngine.Name = "BtnAddEngine";
            BtnAddEngine.Size = new Size(130, 29);
            BtnAddEngine.TabIndex = 0;
            BtnAddEngine.Text = "Elegir Motor";
            BtnAddEngine.UseVisualStyleBackColor = true;
            BtnAddEngine.Click += BtnAddEngine_Click;
            // 
            // BtnAddDriver
            // 
            BtnAddDriver.Location = new Point(445, 132);
            BtnAddDriver.Name = "BtnAddDriver";
            BtnAddDriver.Size = new Size(130, 29);
            BtnAddDriver.TabIndex = 1;
            BtnAddDriver.Text = "Elegir Conductor";
            BtnAddDriver.UseVisualStyleBackColor = true;
            BtnAddDriver.Click += BtnAddDriver_Click;
            // 
            // BtnStartDrive
            // 
            BtnStartDrive.Location = new Point(357, 81);
            BtnStartDrive.Name = "BtnStartDrive";
            BtnStartDrive.Size = new Size(94, 29);
            BtnStartDrive.TabIndex = 6;
            BtnStartDrive.Text = "Conducir";
            BtnStartDrive.UseVisualStyleBackColor = true;
            BtnStartDrive.Click += BtnStartDrive_Click;
            // 
            // RbHuman
            // 
            RbHuman.AutoSize = true;
            RbHuman.Location = new Point(20, 42);
            RbHuman.Name = "RbHuman";
            RbHuman.Size = new Size(87, 24);
            RbHuman.TabIndex = 7;
            RbHuman.TabStop = true;
            RbHuman.Text = "Humano";
            RbHuman.UseVisualStyleBackColor = true;
            // 
            // RbRobot
            // 
            RbRobot.AutoSize = true;
            RbRobot.Location = new Point(113, 42);
            RbRobot.Name = "RbRobot";
            RbRobot.Size = new Size(71, 24);
            RbRobot.TabIndex = 8;
            RbRobot.TabStop = true;
            RbRobot.Text = "Robot";
            RbRobot.UseVisualStyleBackColor = true;
            // 
            // RbElectric
            // 
            RbElectric.AutoSize = true;
            RbElectric.Location = new Point(18, 42);
            RbElectric.Name = "RbElectric";
            RbElectric.Size = new Size(87, 24);
            RbElectric.TabIndex = 9;
            RbElectric.TabStop = true;
            RbElectric.Text = "Electrico";
            RbElectric.UseVisualStyleBackColor = true;
            // 
            // RbCombustion
            // 
            RbCombustion.AutoSize = true;
            RbCombustion.Location = new Point(111, 42);
            RbCombustion.Name = "RbCombustion";
            RbCombustion.Size = new Size(110, 24);
            RbCombustion.TabIndex = 10;
            RbCombustion.TabStop = true;
            RbCombustion.Text = "Combustion";
            RbCombustion.UseVisualStyleBackColor = true;
            // 
            // GboxEngine
            // 
            GboxEngine.Controls.Add(RbCombustion);
            GboxEngine.Controls.Add(RbElectric);
            GboxEngine.Location = new Point(171, 180);
            GboxEngine.Name = "GboxEngine";
            GboxEngine.Size = new Size(227, 89);
            GboxEngine.TabIndex = 11;
            GboxEngine.TabStop = false;
            GboxEngine.Text = "Motor";
            // 
            // GboxDriver
            // 
            GboxDriver.Controls.Add(RbHuman);
            GboxDriver.Controls.Add(RbRobot);
            GboxDriver.Location = new Point(419, 180);
            GboxDriver.Name = "GboxDriver";
            GboxDriver.Size = new Size(219, 89);
            GboxDriver.TabIndex = 12;
            GboxDriver.TabStop = false;
            GboxDriver.Text = "Conductor";
            // 
            // Form1
            // 
            AutoScaleDimensions = new SizeF(8F, 20F);
            AutoScaleMode = AutoScaleMode.Font;
            ClientSize = new Size(800, 450);
            Controls.Add(GboxDriver);
            Controls.Add(GboxEngine);
            Controls.Add(BtnStartDrive);
            Controls.Add(BtnAddDriver);
            Controls.Add(BtnAddEngine);
            Name = "Form1";
            Text = "Form1";
            GboxEngine.ResumeLayout(false);
            GboxEngine.PerformLayout();
            GboxDriver.ResumeLayout(false);
            GboxDriver.PerformLayout();
            ResumeLayout(false);
        }

        #endregion

        private Button BtnAddEngine;
        private Button BtnAddDriver;
        private Button BtnStartDrive;
        private RadioButton RbHuman;
        private RadioButton RbRobot;
        private RadioButton RbElectric;
        private RadioButton RbCombustion;
        private GroupBox GboxEngine;
        private GroupBox GboxDriver;
    }
}
